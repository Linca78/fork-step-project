'use strict';

const gulp = require ('gulp');
const sass = require ('gulp-sass');
const browserSync = require('browser-sync'),
    reload = browserSync.reload;
const minifyjs = require ('gulp-js-minify');
const uglify = require('gulp-uglify-es').default;
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const rigger = require('gulp-rigger');
const sourcemaps = require('gulp-sourcemaps');
const rimraf = require('rimraf');
const uglifycss = require('gulp-uglifycss');
const gulpSequence = require('gulp-sequence')

let path = {
    dist: { //куда складываем
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/'
    },
    src: { //откуда берем
        html: 'src/*.html', //берем все файлы с расширением .html
        js: 'src/js/main.js',// только main файлы
        scss: 'src/scss/style.scss',
        img: 'src/img/**/*.*', // img/**/*.* - все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //за изменением каких файлов следим
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        scss: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: 'dist/',


};
let config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
};
gulp.task('sass', () => {
        gulp.src('src/scss/**/*.scss') // Берем все sass файлы из папки sass и дочерних, если таковые будут
        .pipe(sass())
        .pipe(gulp.dest('src/css'))
});
// DEV Запуск сервера и последующее отслеживание изменений *.js и *.scss файлов в папке src
// При изменении - пересборка и копирование объединенного и минифицированного
// файла styles.min.css или script.min.js в папку dist,
// перезагрузка страницы
gulp.task ('dev', ['reload', 'watch', 'build-files'], () => {

})
gulp.task('watch', () => {
    gulp.watch([path.watch.html], ['html-build']);
    gulp.watch([path.watch.scss], ['style-build']);
    gulp.watch([path.watch.js], ['js-build']);
    gulp.watch([path.watch.img], ['image-build']);
    gulp.watch([path.watch.fonts], ['fonts-build']);

});

gulp.task('html-build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.dist.html))
        .pipe(reload({stream: true}));
});

gulp.task('js-build', () => {
    gulp.src('./src/js/*.js')
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.dist.js)) //Выплюнем готовый файл в dist
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style-build', () => {
    gulp.src('./src/scss/*.scss')
      .pipe(sass())
        .pipe(autoprefixer({ //Добавим вендорные префиксы
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.init())
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.dist.css))
        .pipe(reload({stream: true}));

});

gulp.task('fonts-build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.dist.fonts))
});

gulp.task('image-build', () => {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: true}],
            // use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.dist.img))
        .pipe(reload({stream: true}));
});

gulp.task('reload', function() {
    browserSync(config);
});

gulp.task ('build', ['clean', 'build-files', 'reload'], () => {

})

gulp.task ('build-files', [
    'html-build',
    'js-build',
    'style-build',
    'fonts-build',
    'image-build'
])

gulp.task('clean', cb => {
    rimraf(path.clean, cb)
});

// очистка папки dist
// компиляция scss файлов в css
// добавление вендорных префиксов к CSS свойствам для поддержки последних нескольких версий каждого из браузеров
// конкатенация js и css файлов в один
// минификация итоговых js и css файлов
// копирование минифицированных js и css файлов в папку dist
// оптимизация картинок и копирование их в папку dist
